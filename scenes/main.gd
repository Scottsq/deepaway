extends Node

@onready var _2d_scene = $"2DScene"
var fade_in: bool = true

func _ready() -> void:
	LevelManager.main_scene = _2d_scene
	$Music.volume_db = linear_to_db(0.001)


func _process(delta):
	if $Music.volume_db < 0:
		$Music.volume_db += 13*delta
	if $Music.volume_db > 0:
		$Music.volume_db = 0
