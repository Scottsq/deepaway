extends Area2D

@export var interaction_script_name: String
@export var tilemap: TileMap
@export var tile_position: Vector2i

var interaction_script
var debounce_delay: int = 100
var delay = 0

var _player: Player = null

func _ready() -> void:
	interaction_script = load("res://assets/scripts/other/interaction_%s.gd" % interaction_script_name).new()

func _process(delta):
	if _player == null: return
	$Tooltip.show()
	
	if delay <= 0:
		if Input.is_action_pressed("interact"):
			print("pressed E")			
			interaction_script.call_deferred("run", tilemap, global_position, tile_position, $AudioStreamPlayer)
			delay = debounce_delay
	delay -= 5

func _on_body_entered(body: Node2D) -> void:
	print("ENTERED INTERACTABLE: %s" % body.name)
	if body is Player:
		_player = body


func _on_body_exited(body: Node2D) -> void:
	print("LEFT INTERACTABLE: %s" % body.name)
	if body is Player:
		interaction_script.call("stop", tilemap, global_position, tile_position, $AudioStreamPlayer)
		_player = null
		$Tooltip.hide()
