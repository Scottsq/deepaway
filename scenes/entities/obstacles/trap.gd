extends Node2D

@export var blocksX: int = 0
@export var blocksY: int = 4
@export var delay: float = 0
@export var speed_multiplier: float = 1

var falling: bool = true
var starting_pos: Vector2
var tile_size: int = 64
var is_delayed: bool = false

func _ready() -> void:
	starting_pos = position
	if delay > 0:
		var timer = Timer.new()
		timer.connect("timeout", 
			func(): 
				is_delayed = true
				print(name)
		)
		timer.wait_time = delay
		timer.autostart = true
		timer.one_shot = true
		add_child(timer)
	else: is_delayed = true

func _physics_process(delta):
	if not is_delayed: return;
		
	# Descente
	if falling and not is_done_falling():
		if blocksY > 0: position.y += delta * 500 * speed_multiplier
		if blocksX > 0: position.x += delta * 500 * speed_multiplier
	elif falling:
		falling = false
	
	# Remontée
	if not falling and not is_done_backing():
		if blocksY > 0: position.y -= delta * 100 * speed_multiplier
		if blocksX > 0: position.x -= delta * 100 * speed_multiplier
	elif not falling:
		falling = true

func is_done_falling() -> bool:
	return position.y >= starting_pos.y + (blocksY * tile_size) and position.x >= starting_pos.x + (blocksX * tile_size)

func is_done_backing() -> bool:
	return position <= starting_pos
