extends CharacterBody2D

@export var blocksX: int = 0
@export var blocksY: int = 4
@export var delay: int = 0
@export var speed: int = 100

var falling: bool = true
var starting_pos: Vector2
var tile_size: int = 64
var is_delayed: bool = false
var end_pos: Vector2
var way_back: bool = false

func _ready() -> void:
	starting_pos = position
	end_pos = Vector2(starting_pos.x + (blocksX * tile_size), starting_pos.y + (blocksY * tile_size))
	
	if delay > 0:
		var timer = Timer.new()
		timer.connect("timeout", func(): is_delayed = true)
		timer.autostart = true
		timer.wait_time = delay
		add_child(timer)
	else: is_delayed = true

func _physics_process(delta):
	if not is_delayed: return;
	velocity = Vector2(blocksX * speed, blocksY * speed)
	if position.x > end_pos.x or way_back: 
		velocity *= -1
		way_back = true
	if position.x <= starting_pos.x: way_back = false
	velocity.normalized()
	move_and_slide()
