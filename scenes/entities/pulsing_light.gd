extends PointLight2D

var fade_in: bool = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if fade_in and energy > 0.2: energy -= 0.2 * delta
	elif fade_in and energy < 0.2:
		fade_in = false
	elif energy < 0.6: energy += 0.4 * delta
	elif energy >= 0.6: fade_in = true
