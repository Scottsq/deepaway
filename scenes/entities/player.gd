class_name Player
extends CharacterBody2D


@export var speed: float = 500.0
@export var jump_force: float = -500.0
@export var jump_time: float = 0.25
@export var coyote_time: float = 0.075
@export var gravity_multiplier: float = 3.0
@export var can_play = false

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var is_jumping: bool = false
var jump_timer: float = 0
var coyote_timer: float = 0
var can_control: bool = true
var animations: Array[Sprite2D] = []
var distance: float
var on_ladder: bool = false
var base_speed: float = 500.0
var invincible = 10

@onready var walking_sprite = $WalkingSprites
@onready var jumping_sprite = $JumpingSprites
@onready var idle_sprite = $IdleSprites
@onready var death_sprite = $DeathSprites

@onready var animation_player = $AnimationPlayer

func _ready() -> void:
	animations.append(walking_sprite)
	animations.append(jumping_sprite)
	animations.append(idle_sprite)
	animations.append(death_sprite)

func _physics_process(delta):
	if not can_control: 
		return
	
	if invincible > 0:
		invincible -= 10 * delta
	
	if Input.is_action_pressed("pause") and can_play:
		can_control = false
		
		var settings: Settings = get_node("/root/Main/UIMain/SettingsMenu")
		settings.has_player = true;
		settings.resume_button.show()
		settings.show()
		$PointLight2D.hide()
		get_node("/root/Main/2DScene/CanvasModulate").hide()
	
	if not $PointLight2D.visible and can_control:
		$PointLight2D.show()
		get_node("/root/Main/2DScene/CanvasModulate").show()
		
		
	
	# Add the gravity
	if not is_on_floor() and not is_jumping and not on_ladder:
		velocity.y += gravity * gravity_multiplier * delta
		coyote_timer += delta
	else:
		coyote_timer = 0

	# Handle jump.
	if not on_ladder and Input.is_action_just_pressed("jump") and (is_on_floor() or coyote_timer < coyote_time):
		velocity.y = jump_force
		is_jumping = true
	elif not on_ladder and Input.is_action_pressed("jump") and is_jumping:
		velocity.y = jump_force
	elif on_ladder and Input.is_action_pressed("jump"):
		velocity.y = jump_force/1.5
		
	if is_jumping and Input.is_action_pressed("jump") and jump_timer < jump_time:
		jump_timer += delta
	else:
		is_jumping = false
		jump_timer = 0
		
	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var direction = Input.get_axis("move_left", "move_right")
	if direction:
		velocity.x = direction * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
	
	if velocity.x != 0 and not $WalkAudio.playing and is_on_floor():
		$WalkAudio.play()
	elif velocity.x == 0 or not is_on_floor():
		$WalkAudio.stop()
	
	if is_jumping and not $JumpAudio.playing:
		$JumpAudio.play()
		$WalkAudio.stop()
		
	
	distance = position.x
	$Distance.text = "%sm" % round(position.x / 100)
	
	speed = base_speed + (base_speed * (distance / 50000))

	# Flip the slide to face the direction it is moving
	if direction != 0:
		walking_sprite.flip_h = direction < 0
		jumping_sprite.flip_h = direction < 0
		idle_sprite.flip_h = direction < 0
		
	handle_animation(direction)
	LevelManager.handle_chunks(position)
	
	move_and_slide()

func handle_animation(direction) -> void:
	if abs(direction) > 0.1 and is_on_floor():
		play_animation("walk", walking_sprite)
	elif not is_on_floor() and jump_timer > 0:
		play_animation("jump", jumping_sprite)
	elif is_on_floor():
		play_animation("idle", idle_sprite)
		
func handle_danger() -> void:
	if invincible > 0: return
	invincible = 10;
	print("ded")
	can_control = false
	play_animation("death", death_sprite)
	$WalkAudio.stop()
	$JumpAudio.stop()
	$DieAudio.play()
	await get_tree().create_timer(1.25).timeout
	visible = false
	reset_player()

func play_animation(name: String, sprite: Sprite2D) -> void:
	animation_player.play(name)
	for a in animations:
		a.hide()
	sprite.show()

func reset_player() -> void:
	LevelManager.reset()
	var pPos = LevelManager.loaded_levels[0].player_start.position;
	global_position = Vector2(pPos.x, pPos.y)
	play_animation("idle", idle_sprite)
	visible = true
	can_control = true
	invincible = 10;
