extends Area2D

@export var tilemap: TileMap
@export var tile_position: Vector2i
@export var door_tiles: Array[Vector2]
@export var lever_actioned: Vector2

var _player: Player = null
var delay = 0
var debounce_delay = 100

func _process(delta):
	if _player == null: return
	$Tooltip.show()
	
	if delay <= 0:
		if Input.is_action_pressed("interact"):
			$Lever.play()
			tilemap.set_cell(0, tile_position, 0, lever_actioned)
			for tile in door_tiles:
				tilemap.set_cell(0, tile, 0, Vector2i(0,0), 2)
			delay = debounce_delay
	delay -= 5

func _on_body_entered(body):
	if body is Player:
		_player = body


func _on_body_exited(body):
	if body is Player:
		_player = null
		$Tooltip.hide()
