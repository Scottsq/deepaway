class_name Settings

extends Control

@export var player: Player
@export var resume_button: Button

var has_player: bool = false

func _on_back_button_pressed() -> void: 
	hide()
	if has_player:
		player.can_control = false
		has_player = false
		resume_button.hide()
		get_node("/root/Main/UIMain/MainMenu").show()


func _on_resume_button_pressed() -> void: 
	hide()
	if has_player:
		player.can_control = true
