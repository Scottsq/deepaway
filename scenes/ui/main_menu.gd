class_name MainMenu
extends Control

@onready var player: Player = $"../../Player"

func _ready():
	player.hide()

func _on_play_button_pressed():
	LevelManager.reset()	
	player.show()
	player.global_position = Vector2(200, -200)
	player.can_play = true
	player.can_control = true
	deactivate()


func _on_settings_button_pressed():
	get_node("/root/Main/UIMain/SettingsMenu").show()


func _on_quit_button_pressed():
	get_tree().quit()

func deactivate() -> void:
	hide()
	set_process(false)
	set_physics_process(false)
	set_process_unhandled_input(false)
	set_process_input(false)

func activate() -> void:
	player.hide()
	show()	
	set_process(true)
	set_physics_process(true)
	set_process_unhandled_input(true)
	set_process_input(true)
