func run(tilemap: TileMap, position: Vector2, tile_position: Vector2i) -> void:
	tilemap.set_cell(0, tile_position, 0, Vector2i(11, 11))

func stop(tilemap: TileMap, position: Vector2, tile_position: Vector2i) -> void:
	pass
