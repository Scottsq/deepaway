var is_on_ladder: bool = false

func run(tilemap: TileMap, position: Vector2, tile_position: Vector2i, audio: AudioStreamPlayer) -> void:
	print("ladder.run()")
	var player: Player = tilemap.get_node("/root/Main/Player")
	is_on_ladder = not is_on_ladder
	player.on_ladder = is_on_ladder
	if is_on_ladder and not audio.playing:
		audio.play()
	
func stop(tilemap: TileMap, position: Vector2, tile_position: Vector2i, audio: AudioStreamPlayer) -> void:
	print("ladder.stop()")
	var player: Player = tilemap.get_node("/root/Main/Player")
	player.on_ladder = false
	audio.stop()
