class_name Level
extends Node

@export var level_id: int
@export var level_start: Marker2D
@export var level_end: Marker2D
@export var player_start: Marker2D

var calculated_start_pos: Vector2
var calculated_end_pos: Vector2
var level_data: LevelData

func _ready() -> void:		
	level_data = LevelManager.get_level_data_by_id(level_id)
