extends Node

var levels: Array[LevelData]
var main_scene: Node2D = null
var loaded_levels: Array[Level] = []
var levels_history: Array[int] = []
var current_history_index: int = 0
const LVL_PATH: String = "res://scenes/levels/%s"
var lbl_distance: Label

func unload_level(index) -> void:
	var lvl = loaded_levels[index]
	lvl.queue_free()
	loaded_levels.remove_at(index)

func init_levels_array() -> void:
	var dir = DirAccess.open("res://scenes/levels")
	if dir:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		var counter = 1
		while file_name != "":
			if not dir.current_is_dir():
				levels.append(LevelData.new(file_name, counter))
				counter += 1
			file_name = dir.get_next()

func load_level(index = null) -> void:
	# Si on a un level spécial on spécifie l'index dans le tableau levels
	var new_lvl_index = index;
	# Sinon on choisit un index random en fonction du précédent
	if new_lvl_index == null: 
		# TODO: en fonction du précédent
		new_lvl_index = randi_range(1, levels.size() - 1)
	
	# On récupère les infos du level
	var level_data = levels[new_lvl_index]
	if not level_data: return
	
	# On load le level
	var level_path = LVL_PATH % level_data.level_path
	var level_res := load(level_path)
	if not level_res: return
	
	# On récup la position du level précédent en fonction de si on retourne en arrière ou si on avance
	var lastLevelPos = Vector2.ZERO
	if loaded_levels.size() > 0:
		lastLevelPos = loaded_levels[-1].calculated_end_pos
	var posX = lastLevelPos.x
	var posY = lastLevelPos.y
	
	# On ajoute le level à la scene principale
	var loaded_level: Level = level_res.instantiate()
	loaded_level.level_data = level_data
	main_scene.call_deferred("add_child", loaded_level)
	
	# Et on le position correctement
	loaded_level.calculated_start_pos = Vector2(loaded_level.level_start.position.x + posX, loaded_level.level_start.position.y + posY)
	loaded_level.calculated_end_pos = Vector2(loaded_level.level_end.position.x + posX, loaded_level.level_end.position.y + posY)
	loaded_level.set_position(loaded_level.calculated_start_pos)
	
	# On l'ajoute à une liste qui gère le loading / unloading 
	loaded_levels.append(loaded_level)
	
	# Si c'est un nouveau level on l'ajoute à l'historique
	if current_history_index >= levels_history.size() - 3 or loaded_levels.size() < 5:
		levels_history.append(loaded_level.level_data.level_id)

func get_level_data_by_id(id: int) -> LevelData:
	var level_to_return: LevelData = null
	for lvl: LevelData in levels:
		if lvl.level_id == id:
			level_to_return = lvl
	return level_to_return


func handle_chunks(chunk_pos: Vector2) -> void:
	var filtered_levels: Array[Level] = loaded_levels.filter(func(l: Level): return l.calculated_start_pos.x < chunk_pos.x and l.calculated_end_pos.x >= chunk_pos.x)
	if filtered_levels.size() <= 0: return
	
	var current_level: Level = filtered_levels[0]
	var index = loaded_levels.find(current_level)
	
	if loaded_levels.size() - 4 < index:
		load_level()
	if loaded_levels.size() > 10:
		unload_level(0)

func reset() -> void:
	while loaded_levels.size() > 0:
		unload_level(0)
	if levels.size() == 0:
		init_levels_array()
		levels.push_front(LevelData.new("special/level_01.tscn", 0))
	loaded_levels = []
	levels_history = []
	load_level(0)
	for i in range(5):
		load_level()

func get_current_level(position: Vector2) -> Level:
	var l = loaded_levels.filter(func (lvl: Level): lvl.calculated_start_pos.x <= position.x && lvl.calculated_end_pos.x >= position.x)
	if l.size() > 0: return l[0]
	return null
